package Discord.FridgeDiscord;

import java.awt.Color;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction;

public class SlashEvent extends ListenerAdapter {

	private DatabaseDAO database;

    public SlashEvent() {
		database = new DatabaseDAO();
    }

    public void createSlashCommands(JDA builder) {
        CommandListUpdateAction commands = builder.updateCommands();
        
        commands.addCommands(new CommandData("dette", "Donne votre dette du fridge."));
        // commands.addCommands(
        //         new CommandData("encaisser", "Skip current song.")
        //             .addOptions(new OptionData(OptionType.USER, "utilisateur", "La personne a enchaisser.")
        //                 .setRequired(true))
        //             .addOptions(new OptionData(OptionType.INTEGER, "montant", "La valeur de leur dépense.")
        //                 .setRequired(true)));
        commands.addCommands(new CommandData("inventaire", "Voir l'inventaire avec le prix."));
        commands.addCommands(new CommandData("linkuser", "Set the volume between 0% and 150%.")
                    .addOptions(new OptionData(OptionType.USER, "utilisateur", "L'utilisateur discord.")
                        .setRequired(true))
                    .addOptions(new OptionData(OptionType.STRING, "pseudo", "Pseudo dans le fridge.")
                        .setRequired(true)));
        commands.addCommands(new CommandData("aide", "Voir les commandes possibles."));
        commands.queue();
    }

    @Override
    public void onSlashCommand(SlashCommandEvent event) {
        if (event.getGuild() == null)
            return;

        switch (event.getName()) {
            case "dette":
                debtCommand(event);
                break;
            // case "encaisser":
            //     encaisserCommand(event);
            //     break;
            case "inventaire":
                stockCommand(event);
                break;
            case "linkuser":
				addUserCommand(event);
                break;
            case "aide":
                helpCommand(event);
                break;
        }
    }

    private void helpCommand(SlashCommandEvent event) {
		EmbedBuilder embed = new EmbedBuilder();
		embed.setTitle("Voici les commandes disponible:");
		embed.setDescription(
			"/dette : donne votre solde actuelle.\n"
		// + "/encaisser <@personne a encaisser> <montant $: x.y>  : permet au trésorier.ière d'encaisser un paiement.\n"
		+ "/inventaire : donne une liste du inventaire actuel dans le Fridge.\n"
		+ "/linkuser <@personne à ajouter> <surnom du comic> : permet au trésier.ère d'ajouter un nouveau utilisateur.\n"
		+ "/aide : affiche ce message.\n\n"
		+ "Pour que le.a trésorier.ière puisse faire les commandes, assignez le role \"MoneyMaster\"\n"
		+ "Pour tout problème, veuillez vous rendre sur le git suivant de Torrent: https://gitlab.com/PhilippeBabin/fridgediscord");
		embed.setColor(Color.blue);
		event.replyEmbeds(embed.build()).queue();
    }

    private void debtCommand(SlashCommandEvent event) {
        double number = database.getDebt(event.getUser().getIdLong());
        if (number == -1.0) {
            event.reply("Il y a un problème avec la base de donnée, allez voir les logs!").queue();
            return;
        }
        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle("💰 Votre Dette 💰");
        embed.setDescription("Vous devez au Comic: " + number + " $");
        embed.setColor(Color.RED);
        event.replyEmbeds(embed.build()).queue();
    }

    // private void encaisserCommand(SlashCommandEvent event) {
    //         Member moneyMaster = event.getMember();
    //         if (isUserMoneyMaster(moneyMaster)) {
    //             // TODO: talk to Unity to see how this would work
    //         }

    //     }
    // }

    private void stockCommand(SlashCommandEvent event) {
        Tuple<List<String>,List<Integer>,List<Double>> stock = database.getInventory();
		String[] columnNames = {"Produit", "Inventaire", "Prix"}; 
        if (stock == null) {
			event.reply("Il y a un problème avec la base de donnée, allez voir les logs!").queue();
			return;
		}
		EmbedBuilder embed = new EmbedBuilder();
		embed.setTitle("🛒 Inventaire Disponible 🛒");
		StringBuilder title = new StringBuilder();
		Formatter formatterTitle = new Formatter(title, Locale.US);
		formatterTitle.format("%20s | %10s | %5s", columnNames[0], columnNames[1], columnNames[2]);
		formatterTitle.close();
		String listOfInfo = title.toString();
		for (int i = 0; i <= stock.getFirstValue().size(); i++) {
			StringBuilder sb = new StringBuilder();
			Formatter formatter = new Formatter(sb, Locale.US);
			listOfInfo += formatter.format("%1$-20s | %2$-10d | %3$-5.2f $", stock.getFirstValue().get(i), stock.getSecondValue().get(i), stock.getThiredValue().get(i)) + "\n";
			formatter.close();
		}
		embed.setDescription(listOfInfo);
		embed.setColor(Color.BLUE);
		event.replyEmbeds(embed.build()).queue();
		return;
    }

    private void addUserCommand(SlashCommandEvent event) {
		Long userId = event.getOption("utilisateur").getAsLong();
		String pseudo = event.getOption("pseudo").getAsString();
		if (isUserMoneyMaster(event.getMember())) {
			if (database.linkUser(userId, pseudo)) {
				event.reply("Utilisateur bien linké !").queue();
				return;
			}
			event.reply("Pseudo inconnu ou problème de base de donnée.").queue();
		}
	}

    private boolean isUserMoneyMaster(Member member) {
        for (Role role : member.getRoles()) {
            if (role.getName().equals("MoneyMaster")) {
                return true;
            }
        }
        return false;
    }
}
