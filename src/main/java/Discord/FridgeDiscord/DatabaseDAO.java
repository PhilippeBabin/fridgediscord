package Discord.FridgeDiscord;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.mysql.cj.jdbc.MysqlDataSource;

public class DatabaseDAO {

	public DatabaseDAO() {

	}

	private MysqlDataSource connectToDatabase() {
		Properties dbProperties = new Properties();
		try {
			dbProperties.load(new FileInputStream("./app.properties"));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		String dbUrl = dbProperties.getProperty("DbUrl");
		String dbUsername = dbProperties.getProperty("DbUsername");
		String dbPassword = dbProperties.getProperty("DbPassword");
		String dbName = dbProperties.getProperty("DbName");

		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setServerName(dbUrl);
		dataSource.setUser(dbUsername);
		dataSource.setPassword(dbPassword);
		dataSource.setDatabaseName(dbName);

		createTables(dataSource);

		return dataSource;
	}

	private void createTables(MysqlDataSource dataSource) {
		String sqlDiscordTable = "CREATE TABLE IF NOT EXISTS Discord " +
                "(" +
                "id INT not NULL AUTO_INCREMENT PRIMARY KEY, " +
                " discordId BIGINT not NULL, " +
                " userId INT not NULL" +
                ")";
        
        try (Connection conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();) {
            stmt.executeUpdate(sqlDiscordTable);
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

	public double getDebt(Long userId) {
		String sql = "SELECT dette from Utilisateur, Discord WHERE Discord.discordId = ? AND Utilisateur.id = Discord.userId";
		Double debt = -1.0;
		try (Connection con = connectToDatabase().getConnection();
            PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, userId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
					debt = rs.getDouble("dette");
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return debt;
	}

	public int getUserId(String pseudo) {
		String sql = "SELECT id from Utilisateur WHERE surnom = ?";
		int id = -1;
		try (Connection con = connectToDatabase().getConnection();
            PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, pseudo);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
					id = rs.getInt("id");
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
	}

	private boolean doesUserExists(Long discordId) {
		String sql = "SELECT id from Discord WHERE discordId = ?";
		boolean doesUserExists = false;
		try (Connection con = connectToDatabase().getConnection();
            PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, discordId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
					doesUserExists = true;
				}
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return doesUserExists;
	}

	public boolean linkUser(Long discordId, String pseudo) {
		if (doesUserExists(discordId))
			return false;
		
		int userId = getUserId(pseudo);
		String sql = "INSERT INTO Discord (discordId, userId) VALUES (?, ?)";
		try (Connection con = connectToDatabase().getConnection();
				PreparedStatement ps = con.prepareStatement(sql)) {
				ps.setLong(1, discordId);
				ps.setInt(2, userId);
				ps.execute();
				ps.close();
				con.close();
				return true;
        } catch (SQLException e) {
            e.printStackTrace();
			return false;
        }

		// Statement statement;
		// try {
		// 	PreparedStatement prerequis = connection.prepareStatement(
		// 			"CREATE TABLE IF NOT EXISTS Discord (" + "id INT(6) AUTO_INCREMENT PRIMARY KEY,\r\n"
		// 					+ "discordId BIGINT NOT NULL,\r\n" + "userId INT(6) NOT NULL\r\n" + ");");
		// 	prerequis.execute();
		// 	statement = connection.createStatement();
		// 	statement.executeUpdate("INSERT INTO Discord (discordId, userId) VALUES " + "( (" + discordId + " ),"
		// 			+ " ( " + "SELECT id FROM Utilisateur WHERE Utilisateur.surnom = '" + pseudo + "' ) );");
		// 	return true;
		// } catch (SQLException e) {
		// 	e.printStackTrace();
		// 	return false;
		// }
	}

	public Tuple<List<String>,List<Integer>,List<Double>> getInventory() {
		String sql = "SELECT nom, stock, cout FROM Produit GROUP BY nom;";
		List<String> items = new ArrayList<>();
		List<Integer> stock = new ArrayList<>();
		List<Double> price = new ArrayList<>();
		try (Connection con = connectToDatabase().getConnection();
            PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
					items.add(rs.getString("nom"));
					stock.add(rs.getInt("stock"));
					price.add(rs.getDouble("cout"));
                }
                rs.close();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Tuple<List<String>,List<Integer>,List<Double>>(items, stock, price);
	}
}
